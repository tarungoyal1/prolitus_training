document.getElementById("showAlert").addEventListener("click", function(){
	console.log("Hello world!");
});


mynumber = 50;

myAr = [60, 430, 10, 20, 30];

myArray = new Array("hello", 555, "world", 2000);

document.getElementById("arrayButton").addEventListener("click", function(){
	// adding element to the array at index
	myArray[0] = "Three";

	// push inserts at the end
	// myArray.push(myArray[myArray.length-1]+1000);


	// The shift() method removes the first item of an array.
	// Note: This method changes the length of the array.
	// Note: The return value of the shift method is the removed item.

	// myArray.shift();
	// console.log(myArray);
	// console.log("len = "+myArray.length);
	// console.log(typeof(myArray))


	myArray.forEach(function(d, e){
		console.log(d,e);
	});

	// pop removes the last item of array
	// myArray.pop();
});


document.getElementById("splice").addEventListener("click", function(){
	x = myArray.splice(2, 1);
	console.log(x);

});

// document.getElementById("Unshift").addEventListener("click", function(){
// 	// insert at the beginning of array
// 	myArray.unshift(555);
// 	console.log(myArray);

// });

document.getElementById("Sort").addEventListener("click", function(){
	// insert at the beginning of array
	myAr.sort();
	console.log(myArray);

});

document.getElementById("Concat").addEventListener("click", function(){
	// joining two arrays into one
	myD = new Array(1, 2, 3, 4, 5);
	myE = myArray.concat(myD);
	console.log(myE);

});

document.getElementById("toString").addEventListener("click", function(){
	// convert array to string
	str = myArray.toString();
	console.log(str);

});

document.getElementById("join").addEventListener("click", function(){
	// joining array on a seperator, returns as string
	str = myArray.join('-');
	console.log(str);
});





document.getElementById("showNameAlert").addEventListener("click", function(){
	name = prompt("What is your name?")
	console.log(mynumber);

});

var math_button = document.getElementById("mathbtn");
math_button.onclick = function (){

	var a = document.getElementById("numA").value;
	var b = document.getElementById("numB").value;
	var mathresult = document.getElementById("mathresult");



	var aa = parseInt(a);
	var bb = parseInt(b);

	if (a == '' || b == ''){
		mathresult.innerHTML = "Enter all inputs!";
		return false;
	}
	mathresult.innerHTML = "Result  = " + (aa+bb);
	var status = (aa>bb) ? "<br>A is bigger than B" : "<br>B is bigger than A";
	mathresult.innerHTML += status;
}

var mylist = document.getElementById("mylist");
var myItems = mylist.getElementsByTagName("li");

for(var i=0;i<myItems.length;i++){
	myItems[i].style.backgroundColor = "red";
	myItems[i].style.listStyle = "none";
	myItems[i].style.margin = "10px";
}

console.log(myItems);

var logical_button = document.getElementById("logical");
logical_button.onclick = function (){
	var a = true;
	var b = true;
	document.getElementById("logicalresult").innerHTML = (a&&b);
}

document.getElementById("mathrandom").onclick = function (){

	// var a = parseInt(document.getElementById("numA").value);
	// var b = parseInt(document.getElementById("numB").value);

	var output = Math.random() * 1000;

	document.getElementById("randomResult").innerHTML = output;
}

document.getElementById("dateButton").onclick = function (){
	var myDate = new Date();

	//return in seconds
	var myTime = myDate.getTime();

	document.getElementById("dateResult").innerHTML = myTime;
}

document.getElementById("condition").onclick = function (){

	var a = parseInt(document.getElementById("numA").value);
	var b = parseInt(document.getElementById("numB").value);

	var output = '';
	if(a==b)output = "A is equal to B";
	else if(a>b)output = "A is larger than B";
	else if(b>a)output = "B is larger than A";

	document.getElementById("conditionResult").innerHTML = output;
}

document.getElementById("loopbButton").onclick = runWhileLoop;

function runWhileLoop(){
	var counter = parseInt(document.getElementById("loopA").value);

	// var myContainer = "RUN WHILE LOOP<br>";
	// var i=1;
	// while(i<=counter){
	// 	myContainer+= i + " new number<br>";
	// 	i++;
	// }

	// var myContainer = "RUN DO LOOP<br>";
	// var i=1;
	// do{
	// 	myContainer+= i + " new number<br>";
	// 	i++;
	// }while(i<=counter);

	var myContainer = "RUN FOR LOOP<br>";
	for(var i=1;i<=counter;i++){
		myContainer+= i + " new number<br>";
	}


	document.getElementById("loopResult").innerHTML = myContainer;
}
